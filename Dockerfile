FROM node:10.23.0

WORKDIR /app

RUN npm install -g contentful-cli

COPY package.json package-lock.json ./
RUN npm install

COPY . .

USER node
EXPOSE 3000

CMD ["npm", "run", "start:dev"]
